import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<String> numberList = new ArrayList<>();
        numberList.add("ahat");
        numberList.add("shtime");
        numberList.add("shalosh");
        numberList.add("arba");
        numberList.add("hamesh");
        numberList.add("shesh");
        numberList.add("sheva");
        numberList.add("shmone");
        numberList.add("teisha");
        numberList.add("eser");

        System.out.println(numberList.toString());
        numberList.remove("shmone");
        System.out.println(numberList.toString());
        numberList.remove("ahat");
        System.out.println(numberList.toString());
        numberList.remove("eser");
        System.out.println(numberList.toString());
        numberList.remove("pupup");
        System.out.println(numberList.toString());


        List<String> letterList = new LinkedList<>();
        letterList.add("alef");
        letterList.add("bet");
        letterList.add("gimel");
        letterList.add("dalet");
        letterList.add("hei");
        letterList.add("vav");
        letterList.add("zain");
        letterList.add("het");
        letterList.add("tet");

        System.out.println(letterList.toString());
        letterList.remove("tet");
        System.out.println(letterList.toString());
        letterList.remove("vav");
        System.out.println(letterList.toString());
        letterList.remove("alef");
        System.out.println(letterList.toString());
        letterList.remove("pupup");
        System.out.println(letterList.toString());


    }
}
