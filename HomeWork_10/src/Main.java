import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        
        assert findFrequentestWord("Hello Hello bye Hello bye Inno").equals("Hello 3");
        assert findFrequentestWord("Hello Hello bye Hello bye Inno Inno Inno Inno").equals("Inno 4");
        assert findFrequentestWord("Hello Hello bye hello bye Inno bye").equals("bye 3");
        assert findFrequentestWord("Hello Hello bye Hello Hello bye Hello Hello bye Hello Hello bye").equals("Hello 8");
        assert findFrequentestWord("bye").equals("bye 1");
    }

    public static String findFrequentestWord(String text) {
        String[] words = text.split(" ");
        Map<String, Integer> wordCounter = new HashMap<>();

        String maxKey = words[0];
        int maxCount = 1;
        int currentCount = 1;

        for (String word : words) {
            if (wordCounter.containsKey(word)) {
                currentCount = wordCounter.get(word);
                wordCounter.put(word, currentCount + 1);
                currentCount++;

                if (currentCount > maxCount) {
                    maxKey = word;
                    maxCount = currentCount;
                }
            }  else {
                wordCounter.put(word, 1);
            }
        }

        StringBuilder result = new StringBuilder();

        return result.append(maxKey).append(" ").append(maxCount).toString();

    }
}
