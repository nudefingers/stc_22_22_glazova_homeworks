public class Product {
    private Integer id;
    private String title;
    private double cost;
    private Integer quantity;

    public Product(Integer id, String title, double cost, int quantity) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getCost() {
        return cost;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void update(Product product) {
        this.setCost(product.getCost());
        this.setQuantity(product.getQuantity());
    }
}
