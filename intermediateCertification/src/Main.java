public class Main {
    public static void main(String[] args) {
        ProductsRepositoryFileBasedImpl products = new ProductsRepositoryFileBasedImpl("input.txt");
        assert products.findAllByTitleLike("ро")
                .stream()
                .map(Product::getTitle)
                .toList()
                .toString()
                .equals("[Корова, Ведро]");
        assert products.findAllByTitleLike("РО")
                .stream()
                .map(Product::getTitle)
                .toList()
                .toString()
                .equals("[Корова, Ведро]");
        assert products.findAllByTitleLike("су")
                .stream()
                .map(Product::getTitle)
                .toList()
                .toString()
                .equals("[Сухарик, Косуха]");
        assert products.findAllByTitleLike("кОрОвА")
                .stream()
                .map(Product::getTitle)
                .toList()
                .toString()
                .equals("[Корова]");

        assert products.findById(1).getTitle().equals("Молоко");
        assert products.findById(4).getTitle().equals("Сухарик");

        assert Double.valueOf(products.findById(3).getCost()).equals(0.0);
        assert products.findById(3).getQuantity().equals(0);
        Product bucket = products.findById(3);
        bucket.setCost(4444.44);
        bucket.setQuantity(1919191);
        products.update(bucket);
        assert Double.valueOf(products.findById(3).getCost()).equals(4444.44);
        assert products.findById(3).getQuantity().equals(1919191);

    }
}