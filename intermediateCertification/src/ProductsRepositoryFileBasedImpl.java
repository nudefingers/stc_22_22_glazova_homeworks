import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private Product lineToProductMapper(String currentProduct) {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        double cost = Double.parseDouble(parts[2]);
        Integer quantity = Integer.parseInt(parts[3]);

        return new Product(id, name, cost, quantity);
    }
    private String productToLineMapper(Product product) {
        return product.getId()
                + "|" + product.getTitle()
                + "|" + product.getCost()
                + "|" + product.getQuantity()
                + "\n";
    }

    private List<Product> findAll() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader.lines()
                    .map(this::lineToProductMapper)
                    .toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product findById(Integer id) {
        return findAll()
                .stream()
                .filter(product -> product.getId().equals(id))
                .findFirst()
                .get();
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        return findAll()
                .stream()
                .filter(product -> product.getTitle().toLowerCase().contains(title.toLowerCase()))
                .toList();
    }

    @Override
    public void update(Product product) {
        List<Product> products = findAll();
                products.stream()
                .filter(p -> p.getId().equals(product.getId()))
                .forEach(p -> p.update(product));

        String result = products.stream()
                .map(this::productToLineMapper)
                .collect(Collectors.joining())
                .trim();

        try (FileWriter fileWriter = new FileWriter(fileName, false);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
