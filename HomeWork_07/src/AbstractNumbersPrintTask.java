public abstract class AbstractNumbersPrintTask implements Task {
    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = Math.min(from, to);
        this.to = Math.max(from, to);
    }
}
