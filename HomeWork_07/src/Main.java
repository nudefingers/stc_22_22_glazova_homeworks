import java.util.Random;

public class Main {
    public static void main(String[] args) {

        int number = (int)(Math.random() * 10) + 1;
        Task[] tasks = new Task[number];
        Random random = new Random();
        boolean isEven;

        System.out.println("Количество задач - " + number);

        for (int i = 0; i < tasks.length; i++) {
            isEven = random.nextBoolean();
            if (isEven) {
                tasks[i] = new EvenNumbersPrintTask(randomNumber(), randomNumber());
            } else {
                tasks[i] = new OddNumbersPrintTask(randomNumber(), randomNumber());
            }
        }
        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) {
            AbstractNumbersPrintTask currentTask = (AbstractNumbersPrintTask) task;
            System.out.print("(" + task.getClass() + ") ");
            System.out.print(currentTask.from + "-" + currentTask.to + ": ");
            task.complete();
        }
    }

    public static int randomNumber() {
        return (int)(Math.random() * 100) + 1;
    }
}