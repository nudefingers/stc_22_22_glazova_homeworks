import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        /*Scanner scanner = new Scanner(System.in);
        System.out.println("Задайте левую границу:");
        int from = scanner.nextInt();
        System.out.println("Введите правую границу:");
        int to = scanner.nextInt();

        System.out.println("Сумма числе интарвала = " + calcSumOfRange(from, to));*/

        /*int[] array = {1, 56, 4, 1000, 3, 3, 7, -10};
        System.out.println("Четные числа в заданном массиве: ");
        selectEvenElements(array);*/

        int[] number = {9, 8, 7, 2, 3};
        System.out.println(toInt(number));

    }

    /* Написать функцию, возвращающую сумму чисел в каком-либо интервале, если
    интервал задан неверно (значение левой границы больше, чем правой) - вернуть -1 */
    public static int calcSumOfRange (int from, int to) {
        if (from > to) {
            return -1;
        }

        int sum = 0;

        for (int i = from; i <= to; i++) {
            sum += i;
        }

        return sum;
    }

    /* Написать процедуру, которая для заданного массива выводит все его четные элементы */
    public static void selectEvenElements(int [] a) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.println(a[i]);
            }
        }
    }

    /* Реализовать функцию toInt(), принимающую на вход число, в виде массива цифр
    (например, число 98 723 задано в виде: int[] number = {9, 8, 7, 2, 3}).
    Результатом работы функции должно стать число,
    записанное в одну переменную типа int.*/
    public static int toInt(int[] number) {

        // {9, 8, 7, 2, 3}
        // 98723

        int result = 0;

        for (int i = 1; i <= number.length; i++) {
            result += number[number.length - i] * Math.pow(10, i - 1);
        }
        return result;
    }
}