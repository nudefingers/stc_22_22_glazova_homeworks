import java.util.Arrays;

public class ArraysTasksResolver {

    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        if ((array.length <= to) && (from > to)) {
            System.err.println("Неверные параметры метода");
        }
        System.out.println(Arrays.toString(array));
        System.out.println("from " + from + " to " + to + ":");
        System.out.println("result: " + task.resolve(array, from, to));
        System.out.println("*******************************************");
    }

}
