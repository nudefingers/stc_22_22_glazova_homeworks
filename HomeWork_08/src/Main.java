public class Main {
    public static void main(String[] args) {
        int[] numbers1 = {1, 2, 3, 4, 5, 6, 7};
        int[] numbers2 = {55, 11, 888, 4};
        int[] numbers3 = {0, 0, 0, 0, 0, 0, 1};
        int[] numbers4 = {5455, 655, 4, 565, 6, 64, 46, 888};

        ArrayTask elementsSum = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to ; i++) {
                sum += array[i];
            }
            return sum;
        };

        ArrayTask digitsSumMaxElement = (array, from, to) -> {
            int current = 0;
            for (int i = from; i <= to ; i++) {
                if (array[i] > current) {
                    current = array[i];
                }
            }

            int sum = 0;
            while(current != 0) {
                sum += (current % 10);
                current /= 10;
            }
            return sum;
        };

        System.out.println("_elementsSum_");
        ArraysTasksResolver.resolveTask(numbers1, elementsSum, 0,4);
        ArraysTasksResolver.resolveTask(numbers2, elementsSum, 2,3);
        ArraysTasksResolver.resolveTask(numbers3, elementsSum, 4,6);
        ArraysTasksResolver.resolveTask(numbers4, elementsSum, 2,5);

        System.out.println("_digitsSumMaxElement_");
        ArraysTasksResolver.resolveTask(numbers1, digitsSumMaxElement, 0,4);
        ArraysTasksResolver.resolveTask(numbers2, digitsSumMaxElement, 2,3);
        ArraysTasksResolver.resolveTask(numbers3, digitsSumMaxElement, 4,6);
        ArraysTasksResolver.resolveTask(numbers4, digitsSumMaxElement, 2,5);
    }
}