package ru.inno.cars.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Car {
    private String number;
    private String model;
    private String color;
    private Integer person_id;
}
