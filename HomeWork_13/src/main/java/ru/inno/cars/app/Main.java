package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryFileBasedImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"--action"})
    private String action;
    private final static String ACTION_READ = "read";
    private final static String ACTION_WRITE = "write";
    public static void main(String[] args) {

        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        if (main.action.equals(ACTION_READ)) {
            CarsRepository carsRepository = getCarsRepository();
            carsRepository.read()
                    .stream()
                    .map(Car::toString)
                    .forEach(System.out::println);
        } else if (main.action.equals(ACTION_WRITE)) {
            CarsRepository carsRepository = getCarsRepository();
            carsRepository.write();
        } else {
            System.out.println("Wrong command");
        }
    }

    public static CarsRepository getCarsRepository() {
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        return new CarsRepositoryFileBasedImpl(dataSource);
    }
}