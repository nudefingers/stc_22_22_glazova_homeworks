package ru.inno.cars.repository;

import ru.inno.cars.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

public class CarsRepositoryFileBasedImpl implements CarsRepository{

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into car(model, color, car_number, person_id) " +
            "values (?, ?, ?, ?)";

    private DataSource dataSource;

    public CarsRepositoryFileBasedImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Car> carsRowMapper = row -> {
        try {
            return Car.builder()
                    .number(row.getString("car_number"))
                    .model(row.getString("model"))
                    .color(row.getString("color"))
                    .person_id(row.getInt("person_id"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    private static boolean enterAnotherCar(Scanner scanner) {
        String answer;

        while (true) {
            System.out.println("Enter another car? [y/n]");
            answer = scanner.nextLine();
            if (answer.equals("y")) {
                return true;
            }
            if (answer.equals("n")) {
                return false;
            } else {
                System.out.println("Wrong command");
            }
        }
    }

    @Override
    public List<Car> read() {
        List<Car> cars = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = carsRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
        return cars;
    }

    @Override
    public void write() {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            Scanner scanner = new Scanner(System.in);
            boolean is_enter = true;

            while (is_enter) {

                System.out.println("Enter model");
                preparedStatement.setString(1, scanner.nextLine());

                System.out.println("Enter color");
                preparedStatement.setString(2, scanner.nextLine());

                System.out.println("Enter car_number");
                preparedStatement.setString(3, scanner.nextLine());

                System.out.println("Enter person_id");
                preparedStatement.setInt(4, scanner.nextInt());

                int affectedRows = preparedStatement.executeUpdate();
                System.out.println("Car inserted");

                if (affectedRows != 1) {
                    throw new SQLException("Can't insert car");
                }
                is_enter = enterAnotherCar(scanner);
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
