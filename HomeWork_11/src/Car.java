public class Car {
    private String number;
    private String model;
    private String color;
    private Integer mileage;
    private Integer cost;

    public Car(String number, String model, String color, int mileage, int cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.cost = cost;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Integer getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return number + "|" +
                model + "|" +
                color + "|" +
                mileage + "|" +
                cost;
    }
}
