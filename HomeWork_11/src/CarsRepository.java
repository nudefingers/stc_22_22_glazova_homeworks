import java.util.List;

public interface CarsRepository {
    List<String> findAllCarsNumbersPresetColorWithoutMileage(String color);
    int countUniqueModelsInPriceRange(int from, int to);
    String printCarColorWithMinCost();
    double calcAverageModelCost(String model);
}
