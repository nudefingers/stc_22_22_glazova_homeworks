public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("input.txt");

        assert carsRepository.findAllCarsNumbersPresetColorWithoutMileage("black").toString()
                .equals("[o004aa411, o010aa000]");
        assert 3 == carsRepository.countUniqueModelsInPriceRange(700, 800);
        assert carsRepository.printCarColorWithMinCost().equals("Green");
        assert 27589 == carsRepository.calcAverageModelCost("Camry");
    }
}