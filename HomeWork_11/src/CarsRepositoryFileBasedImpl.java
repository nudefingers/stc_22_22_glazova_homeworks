import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.List;

public class CarsRepositoryFileBasedImpl implements CarsRepository{

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private Car stringToCarMapper(String currentCar) {
        String[] parts = currentCar.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer cost = Integer.parseInt(parts[4]);

        return new Car(number, model, color, mileage, cost);
    }

    private List<Car> findAll() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader.lines()
                    .map(this::stringToCarMapper)
                    .toList();
        } catch (IOException e) {
            throw new RuntimeException("Caught IOException: " + e.getMessage());
        }
    }

    @Override
    public List<String> findAllCarsNumbersPresetColorWithoutMileage(String color) {
        return findAll()
                .stream()
                .filter(car -> car.getColor().equalsIgnoreCase(color))
                .filter(car -> car.getMileage().equals(0))
                .map(Car::getNumber)
                .toList();
    }

    @Override
    public int countUniqueModelsInPriceRange(int from, int to) {
        return (int) findAll()
                .stream()
                .filter(car -> car.getCost() >= from && car.getCost() <= to)
                .count();
    }

    @Override
    public String printCarColorWithMinCost() { // делаю
        return findAll()
                .stream()
                .min(Comparator.comparing(Car::getCost))
                .map(Car::getColor)
                .get();
    }

    @Override
    public double calcAverageModelCost(String model) {
        return findAll()
                .stream()
                .filter(car -> car.getModel().equalsIgnoreCase(model))
                .mapToInt(Car::getCost)
                .average()
                .getAsDouble();
    }
}
