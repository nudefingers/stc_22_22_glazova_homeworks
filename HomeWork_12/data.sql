insert into person (first_name, last_name, phone, experience, age, is_driver, drive_category, rating)
values ('Люк',    'Бессон',  '89112771501', 10, 60, true,  'ABC', 4),
       ('Жан',    'Рено',    '89213331735', 0,  55, false, '-',   0),
       ('Натали', 'Портман', '89215854474', 2,  21, true,  'B',   2),
       ('Гари',   'Олдман',  '89286663212', 0,  30, false, '-',   0),
       ('Дэнни',  'Айелло',  '89117418585', 22, 41, true,  'B',   5);

insert into car (model, color, car_number, person_id)
values ('Renault Logan',   'Black',  'DA-444-HX', 1),
       ('Citroen Jumpy',   'Blue',   'BP-584-DR', 2),
       ('Peugeot 107',     'White',  'FF-123-LA', 2),
       ('Bugatti Chiron',  'Red',    'AA-007-AA', 2),
       ('Citroen C4',      'Pink',   'WW-111-WW', 3),
       ('Peugeot 3008',    'Green',  'PA-155-SA', 5),
       ('Peugeot Partner', 'Yellow', 'FF-587-FF', 5),
       ('Renault Kaptur',  'Lemon',  'WW-778-DR', 5);

insert into ride (ride_date, duration, person_id, car_id)
values ('2022-07-01', '09:00', 4, 1),
       ('2022-07-01', '24:00', 4, 2),
       ('2022-07-07', '08:00', 1, 4),
       ('2022-07-07', '02:00', 2, 8),
       ('2022-08-01', '00:30', 3, 7),
       ('2022-08-07', '05:50', 4, 3),
       ('2022-09-21', '01:00', 1, 4),
       ('2022-09-21', '24:00', 1, 4),
       ('2022-09-21', '12:00', 4, 5),
       ('2022-10-07', '24:00', 4, 6),
       ('2022-11-07', '03:00', 5, 6),
       ('2022-11-17', '09:00', 3, 3),
       ('2022-11-17', '24:00', 1, 2);
