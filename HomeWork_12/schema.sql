drop table if exists ride;
drop table if exists car;
drop table if exists person;

create table person
(
    id             serial primary key,
    first_name     char(20),
    last_name      char(20),
    phone          char(20) unique,
    experience     integer default 0,
    age            integer check (age >= 0 and age <= 120) not null,
    is_driver      bool,
    drive_category char(10) default '-',
    rating         integer check (rating >= 0 and rating <= 5) default 0
);

create table car
(
    id         serial primary key,
    model      char(20),
    color      char(10),
    car_number char(20),
    person_id  integer not null,
    foreign key (person_id) references person (id)
);

create table ride
(
    id         serial primary key,
    ride_date  timestamp not null,
    duration   time,
    person_id  integer not null,
    car_id     integer not null,
    foreign key (person_id) references person(id),
    foreign key (car_id) references car(id)
);
