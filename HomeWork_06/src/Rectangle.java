public class Rectangle extends Shape{

    private int sideA;
    private int sideB;

    public Rectangle(int x, int y, int sideA, int sideB) {
        super(x, y);
        setSideA(sideA);
        setSideB(sideB);
    }

    public int getSideA() {
        return sideA;
    }
    public int getSideB() {
        return sideB;
    }

    private void setSideA(int sideA) {
        this.sideA = sideA;
    }

    private void setSideB(int sideB) {
        this.sideB = sideB;
    }

    @Override
    public double calcPerimeter() {
        return (getSideA() + getSideB()) * 2;
    }

    @Override
    public double calcArea() {
        return getSideA() * getSideB();
    }
}
