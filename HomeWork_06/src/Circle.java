public class Circle extends Ellipse{

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    public int getRadius() {
        return getRadiusSmall();
    }
}
