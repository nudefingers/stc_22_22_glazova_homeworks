public class Ellipse extends Shape{
    private int radiusSmall;
    private int radiusLarge;


    public Ellipse(int x, int y, int radius1, int radius2) {
        super(x, y);
        setRadiusSmall(Math.min(radius1, radius2));
        setRadiusLarge(Math.max(radius1, radius2));
    }

    public int getRadiusSmall() {
        return radiusSmall;
    }

    public int getRadiusLarge() {
        return radiusLarge;
    }

    private void setRadiusSmall(int radiusSmall) {
        this.radiusSmall = radiusSmall;
    }

    private void setRadiusLarge(int radiusLarge) {
        this.radiusLarge = radiusLarge;
    }

    @Override
    public double calcPerimeter() {
        return 4 * (Math.PI * radiusLarge * radiusSmall + (radiusLarge - radiusSmall) * (radiusLarge - radiusSmall)) / (radiusLarge + radiusSmall);
    }

    @Override
    public double calcArea() {
        return (Math.PI * radiusSmall * radiusLarge);
    }
}
