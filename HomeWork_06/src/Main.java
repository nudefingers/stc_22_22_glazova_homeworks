import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {
        testShape();
        testRectangle();
        testSquare();
        testEllipse();
        testCircle();
    }

    public static void testShape() {
        Shape shape = new Shape(3, 3);
        assert (double) 0 == shape.calcArea();
        assert (double) 0 == shape.calcPerimeter();

        shape.move(10, -5);
        assert 10 == shape.getX();
        assert -5 == shape.getY();

        System.out.println("Shape is OK");
    }

    public static void testRectangle() {
        Rectangle rectangle = new Rectangle(5, 5, 2, 10);
        assert (double) 20 == rectangle.calcArea();
        assert (double) 24 == rectangle.calcPerimeter();

        rectangle.move(100, 100);
        assert 100 == rectangle.getX();
        assert 100 == rectangle.getY();

        System.out.println("Rectangle is OK");
    }

    public static void testSquare() {
        Square square = new Square(7, 7, 8);
        assert (double) 64 == square.calcArea();
        assert (double) 32 == square.calcPerimeter();

        square.move(10, -5);
        assert 10 == square.getX();
        assert -5 == square.getY();

        System.out.println("Square is OK");
    }

    public static void testEllipse() {
        Ellipse ellipse = new Ellipse(2, 2, 5, 10);
        assert 50 * Math.PI == ellipse.calcArea();
        assert String.format("%.3f", ellipse.calcPerimeter()).equals(String.format("%.3f", 48.554568714531));

        ellipse.move(50, -1);
        assert 50 == ellipse.getX();
        assert -1 == ellipse.getY();

        System.out.println("Ellipse is OK");
    }

    public static void testCircle() {
        Circle circle = new Circle(3, 3, 4);
        assert 16 * Math.PI == circle.calcArea();
        assert String.format("%.3f", circle.calcPerimeter()).equals(String.format("%.3f", 25.132741228718));

        circle.move(10, 10);
        assert 10 == circle.getX();
        assert 10 == circle.getY();

        System.out.println("Circle is OK");
    }
}