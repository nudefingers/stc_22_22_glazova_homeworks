public class Shape {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private void setX(int x) {
        this.x = x;
    }

    private void setY(int y) {
        this.y = y;
    }

    public Shape(int x, int y) {
        setX(x);
        setY(y);
    }

    public double calcPerimeter() {
        return 0;
    }

    public double calcArea() {
        return 0;
    }

    public void move(int toX, int toY) {
        setX(toX);
        setY(toY);
    }
}
