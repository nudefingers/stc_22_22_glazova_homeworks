package ru.inno.ec.services;

import ru.inno.ec.models.Category;
import ru.inno.ec.models.User;

import java.util.List;

public interface CategoriesService {
    //void addStudentToCourse(Long courseId, Long studentId);

    List<Category> getAllCategories();

    //Course getCourse(Long courseId);

    //List<User> getNotInCourseStudents(Long courseId);

    //List<User> getInCourseStudents(Long courseId);
}
