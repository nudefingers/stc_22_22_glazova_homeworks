package ru.inno.ec.services;

import ru.inno.ec.dto.BookForm;
import ru.inno.ec.dto.CategoryForm;
import ru.inno.ec.models.Book;

import java.util.List;

public interface BooksService {
    List<Book> getAllBooks();

    List<Book> getBooksByAuthor(String author);

    void addBook(BookForm book);

    Book getBook(Long id);

    void updateBook(Long bookId, BookForm book);

    void deleteBook(Long bookId);

    void addCategoryToBook(Long bookId, CategoryForm category);

    List<String> getAllAuthors();

}
