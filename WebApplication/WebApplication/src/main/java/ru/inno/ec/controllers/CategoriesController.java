package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.services.BooksService;
import ru.inno.ec.services.CategoriesService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/categories")
public class CategoriesController {

    private final BooksService booksService;
    private final CategoriesService categoriesService;

    @GetMapping
    public String getCategoriesPage(Model model) {
        model.addAttribute("categories", categoriesService.getAllCategories());
        return "categories/categories_page";
    }

    @GetMapping("/{category-id}")
    public String getCategoryPage(@PathVariable("category-id") Long categoryId, Model model) {
        //model.addAttribute("category", categoriesService.getC(categoryId));

        //model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(categoryId));
        //model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(categoryId));
        return "categories/category_page";
    }
}
