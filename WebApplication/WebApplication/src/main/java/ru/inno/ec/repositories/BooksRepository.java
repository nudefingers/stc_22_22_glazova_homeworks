package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
//import ru.inno.ec.models.Course;
import ru.inno.ec.models.Book;

import java.util.List;
import java.util.Optional;

public interface BooksRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByStateNot(Book.State state);

    List<Book> findAllByAuthor(String author);

    Optional<Book> findByTitle(String title);
}
