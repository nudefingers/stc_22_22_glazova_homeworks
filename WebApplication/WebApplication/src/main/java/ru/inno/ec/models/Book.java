package ru.inno.ec.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"categories"})
@ToString(exclude = {"categories"})
@Builder
@Entity
public class Book {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column
    private String author;

    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    private Set<Category> categories;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
