package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.BookForm;
import ru.inno.ec.dto.CategoryForm;
import ru.inno.ec.models.Book;
import ru.inno.ec.models.Category;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.BooksRepository;
import ru.inno.ec.repositories.CategoriesRepository;
import ru.inno.ec.services.BooksService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BooksServiceImpl implements BooksService {

    private final BooksRepository booksRepository;
    private final CategoriesRepository categoriesRepository;

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByStateNot(Book.State.DELETED);
    }

    @Override
    public List<Book> getBooksByAuthor(String author) {
        return booksRepository.findAllByAuthor(author);
    }

    @Override
    public void addBook(BookForm book) {
        Book newBook = Book.builder()
                .title(book.getTitle())
                .author(book.getAuthor())
                .state(Book.State.NOT_CONFIRMED)
                .build();

        booksRepository.save(newBook);
    }

    @Override
    public Book getBook(Long id) {
        return booksRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateBook(Long bookId, BookForm updateData) {
        Book bookForUpdate = booksRepository.findById(bookId).orElseThrow();

        bookForUpdate.setTitle(updateData.getTitle());
        bookForUpdate.setAuthor(updateData.getAuthor());

        booksRepository.save(bookForUpdate);
    }

    @Override
    public void deleteBook(Long bookId) {
        Book bookForDelete = booksRepository.findById(bookId).orElseThrow();
        bookForDelete.setState(Book.State.DELETED);

        booksRepository.save(bookForDelete);
    }

    @Override
    public void addCategoryToBook(Long bookId, CategoryForm data) {
        Book book = booksRepository.findById(bookId).orElseThrow();

        Category category = Category.builder()
                .name(data.getName())
                .summary(data.getSummary())
                .book(book)
                .build();

        categoriesRepository.save(category);
    }

    @Override
    public List<String> getAllAuthors() {
        return booksRepository.findAllByStateNot(Book.State.DELETED)
                .stream()
                .map(Book::getAuthor)
                .distinct()
                .toList();
    }
}
