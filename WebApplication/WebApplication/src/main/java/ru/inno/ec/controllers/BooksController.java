package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.ec.dto.BookForm;
import ru.inno.ec.dto.CategoryForm;
import ru.inno.ec.services.BooksService;

@RequiredArgsConstructor
@Controller
public class BooksController {

    private final BooksService booksService;

    @GetMapping("/books")
    public String getBooksPage(Model model) {
        model.addAttribute("books", booksService.getAllBooks());
        return "books/books_page";
    }

    @PostMapping("/books")
    public String addBook(BookForm book) {
        booksService.addBook(book);
        return "redirect:/books";
    }

    @GetMapping("/books/{book-id}")
    public String getBookPage(@PathVariable("book-id") Long id, Model model) {
        model.addAttribute("book", booksService.getBook(id));
        return "books/book_page";
    }

    @GetMapping("/books/{book-id}/categorize")
    public String addCategoryPage(@PathVariable("book-id") Long id, Model model) {
        model.addAttribute("book", booksService.getBook(id));
        return "books/add-category_page";
    }

    @PostMapping("/books/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Long bookId, BookForm book) {
        booksService.updateBook(bookId, book);
        return "redirect:/books/" + bookId;
    }

    @PostMapping("/books/{book-id}/categorize")
    public String categorizeBook(@PathVariable("book-id") Long bookId, CategoryForm category) {
        booksService.addCategoryToBook(bookId, category);
        return "redirect:/books/";
    }

    @GetMapping("/books/{book-id}/delete")
    public String updateBook(@PathVariable("book-id") Long bookId) {
        booksService.deleteBook(bookId);
        return "redirect:/books/";
    }

    @GetMapping("/authors")
    public String getAuthors(Model model) {
        model.addAttribute("authors", booksService.getAllAuthors());
        return "authors/authors_page";
    }

    @GetMapping("/authors/{author}/select")
    public String selectBooksByAuthor(@PathVariable("author") String author, Model model) {
        model.addAttribute("books", booksService.getBooksByAuthor(author));
        return "books/books_page";
    }
}
