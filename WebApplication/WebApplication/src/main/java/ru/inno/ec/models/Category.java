package ru.inno.ec.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "book")
@ToString(exclude = "book")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(length = 1000)
    private String summary;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;
}
