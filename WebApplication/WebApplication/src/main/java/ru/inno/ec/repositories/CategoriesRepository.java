package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Category;

public interface CategoriesRepository extends JpaRepository<Category, Long> {
    Category findAllById(Long categoryId);
}
