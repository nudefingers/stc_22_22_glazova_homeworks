package ru.inno.shop.repositories.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.inno.shop.models.User;
import ru.inno.shop.repositories.UsersRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RequiredArgsConstructor
@Repository
public class UsersRepositoryJdbcImpl implements UsersRepository {

    private final JdbcTemplate jdbcTemplate;

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account";
    //language=SQL
    private static final String SQL_INSERT = "insert into account (email, password) values (?, ?)";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from account where id = ?";

    private final static RowMapper<User> userRowMapper = (row, rowNum) ->
            User.builder()
                    .id(row.getLong("id"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .build();

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(User newUser) {
        jdbcTemplate.update(SQL_INSERT, newUser.getEmail(), newUser.getPassword());
    }

    @Override
    public User findById(Long id) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapper, id);
    }
}
