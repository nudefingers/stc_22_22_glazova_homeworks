package ru.inno.shop.repositories;

import ru.inno.shop.models.User;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();

    void save(User newUser);

    User findById(Long id);
}


