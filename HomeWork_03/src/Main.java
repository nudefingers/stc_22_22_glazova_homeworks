import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Пример: 10 = 12 6 8 11 22 5 8 15 22 0
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива:");
        int size = scanner.nextInt();
        int[] array = new int[size];
        int total = 0;

        System.out.println("Введите элементы массива:");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        // a[i - 1] > a[i] < a[i + 1]

        if (array[0] < array[1]) {
            total++;
        }

        for (int i = 1; i < size - 1; i++) {
            if (array[i - 1] > array[i] && array[i] < array[i + 1]) {
                total++;
            }
        }

        if (array[size - 1] < array[size - 2]) {
            total++;
        }

        System.out.println("Количество локальных минимумов: " + total);
    }
}