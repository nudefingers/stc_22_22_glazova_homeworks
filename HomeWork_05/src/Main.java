public class Main {
    public static void main(String[] args) {
        testATMConstructor();
        testATMGiveMoney();
        testATMPutMoney();
        testATMConductedActivitiesAndBalance();
    }

    public static void testATMConstructor() {
        CashDispenser atm = new CashDispenser(0, 0, 0);
        assert 0 == atm.getAmountMoneyMax();
        assert 0 == atm.getIssuedMoneyMax();

        CashDispenser atm1 = new CashDispenser(1, 0, 2);
        assert 2 == atm1.getAmountMoneyMax();
        assert 0 == atm1.getIssuedMoneyMax();

        atm = new CashDispenser(0, 1, 0);
        assert 0 == atm.getAmountMoneyMax();
        assert 1 == atm.getIssuedMoneyMax();

        atm = new CashDispenser(1, 2, 3);
        assert 3 == atm.getAmountMoneyMax();
        assert 2 == atm.getIssuedMoneyMax();

    }

    public static void testATMGiveMoney() {
        assert 0 == new CashDispenser(0, 2, 0).giveMoney(0);
        assert 0 == new CashDispenser(0, 2, 0).giveMoney(1);
        assert 0 == new CashDispenser(0, 2, 1).giveMoney(0);
        assert 0 == new CashDispenser(0, 2, 1).giveMoney(1);
        assert 0 == new CashDispenser(1, 2, 0).giveMoney(0);
        assert 0 == new CashDispenser(1, 2, 0).giveMoney(1);
        assert 0 == new CashDispenser(1, 2, 1).giveMoney(0);
        assert 1 == new CashDispenser(1, 2, 1).giveMoney(1);

        assert 0 == new CashDispenser(0, 3, 3).giveMoney(2);
        assert 2 == new CashDispenser(2, 3, 3).giveMoney(2);
        assert 2 == new CashDispenser(3, 3, 3).giveMoney(2);
        assert 3 == new CashDispenser(5, 3, 3).giveMoney(3);
    }

    public static void testATMPutMoney() {
        assert 0 == new CashDispenser(2, 0, 0).putMoney(0);
        assert 1 == new CashDispenser(2, 0, 0).putMoney(1);
        assert 0 == new CashDispenser(2, 1, 0).putMoney(0);
        assert 0 == new CashDispenser(2, 1, 3).putMoney(1);
        assert 0 == new CashDispenser(2, 1, 1).putMoney(0);
        assert 1 == new CashDispenser(2, 1, 1).putMoney(1);

        assert 2 == new CashDispenser(2, 0, 0).putMoney(2);
        assert 1 == new CashDispenser(0, 1, 2).putMoney(1);
        assert 2 == new CashDispenser(2, 1, 1).putMoney(2);
    }

    public static void  testATMConductedActivitiesAndBalance() {
        CashDispenser atm = new CashDispenser(2, 2, 2);

        assert 0 == atm.putMoney(0);
        assert 2 == atm.getBalance();
        assert 1 == atm.getNumberOfOperations();

        assert 0 == atm.giveMoney(0);
        assert 2 == atm.getBalance();
        assert 2 == atm.getNumberOfOperations();

        assert 1 == atm.putMoney(1);
        assert 2 == atm.getBalance();
        assert 3 == atm.getNumberOfOperations();

        assert 2 == atm.giveMoney(3);
        assert 0 == atm.getBalance();
        assert 4 == atm.getNumberOfOperations();

        assert 0 == atm.giveMoney(1);
        assert 0 == atm.getBalance();
        assert 5 == atm.getNumberOfOperations();

        System.out.println("_____");
        System.out.println(atm.getBalance());
        System.out.println("_____");

        assert 1 == atm.putMoney(3);
        assert 2 == atm.getBalance();
        assert 6 == atm.getNumberOfOperations();
    }
}