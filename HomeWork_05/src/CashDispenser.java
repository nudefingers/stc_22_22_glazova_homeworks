public class CashDispenser {

    private int balance;
    private int issuedMoneyMax;
    private int amountMoneyMax;
    private int numberOfOperations;

    public CashDispenser(int balance, int issuedMoneyMax, int amountMoneyMax) {
        this.issuedMoneyMax = issuedMoneyMax;
        this.amountMoneyMax = amountMoneyMax;
        if (balance <= amountMoneyMax) {
            this.balance = balance;
        } else {
            this.balance = amountMoneyMax;
            System.out.println("Максимальный объем денег в банкомате = " + amountMoneyMax + ". \nОставшиеся " + (balance - amountMoneyMax) + " положить в банкомат нельзя.");
        }
        this.numberOfOperations = 0;
    }

    public int getBalance() {
        return balance;
    }

    public int getIssuedMoneyMax() {
        return issuedMoneyMax;
    }

    public int getAmountMoneyMax() {
        return amountMoneyMax;
    }

    public int getNumberOfOperations() {
        return numberOfOperations;
    }

    public int giveMoney(int sum) {
        System.out.println("Запрошено для выдачи: " + sum);
        if (sum <= issuedMoneyMax && sum <= balance) {
            System.out.println("Выдано: " + sum);
            balance -= sum;
            numberOfOperations++;
            return sum;
        }
        if (sum > issuedMoneyMax && sum <= balance) {
            System.out.println("Запрос превышает максимальную разрешенную к выдаче сумму.\nВыдано: " + issuedMoneyMax);
            balance -= issuedMoneyMax;
            numberOfOperations++;
            return issuedMoneyMax;
        }
        if (sum <= issuedMoneyMax && sum > balance) {
            System.out.println("В банкомате не хватает денег.\nВыдано: " + balance);
            balance = 0;
            numberOfOperations++;
            return balance;
        }
        if (sum > issuedMoneyMax && sum > balance) {
            sum = Math.min(issuedMoneyMax, balance);
            System.out.println("Запрос превышает установленные лимиты.\nВыдано: " + sum);
            balance -= Math.min(issuedMoneyMax, balance);
            numberOfOperations++;
            return sum;
        }

        return 0;
    }

    public int putMoney(int sum) {

        if (sum + balance < amountMoneyMax) {
            System.out.println("На счет положено: " + sum);
            balance += sum;
            numberOfOperations++;
        } else {
            sum -= (amountMoneyMax - balance);
            System.out.println("Объем превышен на " + sum + "\nНа счет положено: " + (amountMoneyMax - balance));
            balance = amountMoneyMax;
            numberOfOperations++;
        }
        return sum;
    }
}
